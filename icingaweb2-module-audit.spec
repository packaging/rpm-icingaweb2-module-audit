# Audit Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name audit

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        1.0.1
Release:        %{revision}%{?dist}
Summary:        Audit - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

%description
Audit logging module for Icinga Web 2.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md LICENSE

%defattr(-,root,root)
%{basedir}

%changelog
* Mon Sep 09 2019 Markus Frosch <markus.frosch@icinga.com> - 1.0.1-1
- Release version 1.0.1-1

* Wed Aug 28 2019 Markus Frosch <markus.frosch@icinga.com> - 1.0.1-1
- Initial package version
